﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_car : MonoBehaviour {

    public Vector3 startPos;
    public Vector3 moveSpeed;
    public float lifeTime;
    private float lifeTimer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        lifeTimer -= Time.deltaTime;
        transform.Translate(moveSpeed);
        if (lifeTimer < 0)
        {
            transform.position = startPos;
            lifeTimer = lifeTime;
        }
	}
}
