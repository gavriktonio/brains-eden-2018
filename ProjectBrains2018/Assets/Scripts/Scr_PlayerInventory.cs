﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PlayerInventory : MonoBehaviour {

    public float tossAngle;
    public float tossForce;
    public bool isHolding;
    private Scr_ItemBase heldItem;
    private bool canPickUp;
    private List<Scr_ItemBase> availableItems = new List<Scr_ItemBase>();
    public Collider pickUpRange;
    private Animator animator_;

    private Scr_PlayerID id;

    public string aButton_;
    public string bButton_;
    public string cButton_;
    public string dButton_;
    public string triggers_;

    // Use this for initialization
    void Start () {
        id = GetComponent<Scr_PlayerID>();
        SetupControls(id.id);
        animator_ = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (heldItem == null)
        {
            isHolding = false;
        }
        Debug.Log(heldItem);
        if (Input.GetButtonDown(aButton_))
        {
            Debug.Log(id.id);
            if (isHolding)
                DropItem();
            else if (availableItems.Count > 0)
                TakeItem();
        }

        if(isHolding)
        {
            if (Input.GetButton(cButton_))
                TossItem();
            if (Input.GetAxis(triggers_) < -0.5)
                UseItem();
        }
       
        /*
        // input test code
        if (Input.GetButtonDown("FaceButtonA" + id.id))
            print("cross" + id.id);
        if (Input.GetButtonDown("FaceButtonB" + id.id))
            print("circle" + id.id);
        if (Input.GetButtonDown("FaceButtonC" + id.id))
            print("square" + id.id);
        if (Input.GetButtonDown("FaceButtonD" + id.id))
            print("triangle" + id.id);
        if (Input.GetButtonDown("LeftBumper" + id.id))
            print("left bumper" + id.id);
        if (Input.GetButtonDown("RightBumper" + id.id))
            print("right bumper" + id.id);
        if (Input.GetAxis("Triggers" + id.id) > 0.5f)
            print("left trigger" + id.id);
        if (Input.GetAxis("Triggers" + id.id) < -0.5f)
            print("right trigger" + id.id);
        */

    }

    void TakeItem()
    {
        animator_.SetBool("hasGun", true);
        Scr_ItemBase item = GetClosestItem(); 
        item.GetPickedUp(id);

        //check if jammed
        if (item.GetComponent<Scr_GunBase>().gunState == GunStates.jammed)
            GetComponent<Scr_PlayerUI>().ShowMinigameUI(true);

        heldItem = item;
        isHolding = true;
    }

    void DropItem()
    {
        animator_.SetBool("hasGun", false);
        Scr_ItemBase item = heldItem;
        Debug.Log(item);
        item.GetDropped();

        //hide ui
        GetComponent<Scr_PlayerUI>().ShowMinigameUI(false);

        isHolding = false;
        heldItem = null;
    }

    void UseItem()
    {
        heldItem.UseItem(id.id, transform.rotation);
    }

    void TossItem()
    {
        Scr_ItemBase item = heldItem;
        item.GetDropped();
        item.GetTossed((new Vector3(0,tossAngle,0) + transform.forward).normalized, tossForce);

        //hide ui
        GetComponent<Scr_PlayerUI>().ShowMinigameUI(false);

        isHolding = false;
        heldItem = null;
    }

    public Scr_ItemBase GetClosestItem()
    {
        //remove destroyed guns
        for (int i = 0; i < availableItems.Count; i++)
        {
            if (availableItems[i] == null)
                availableItems.RemoveAt(i);
        }

        Scr_ItemBase closestItem = availableItems[0];
        float itemDis = Vector3.Distance(transform.position, availableItems[0].transform.position);
       
        //check whats the closest gun
        for (int i = 1; i < availableItems.Count; i++)
        {
            float newDis = Vector3.Distance(transform.position, availableItems[i].transform.position);
            if (newDis < itemDis)
                closestItem = availableItems[i];
        }
        return (closestItem);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "ItemTrigger")
        {
            availableItems.Add(other.gameObject.GetComponent<Scr_ItemBase>());
            canPickUp = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "ItemTrigger")
        {
            availableItems.Remove(other.gameObject.GetComponent<Scr_ItemBase>());
            if (availableItems.Count == 0)
                canPickUp = false;
        }
    }

    void SetupControls(int playerId)
    {
        aButton_ = "FaceButtonA" + playerId;
        bButton_ = "FaceButtonB" + playerId;
        cButton_ = "FaceButtonC" + playerId;
        dButton_ = "FaceButtonD" + playerId;
        triggers_ = "Triggers" + playerId;
    }
}
