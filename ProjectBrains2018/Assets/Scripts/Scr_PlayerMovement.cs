﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class Scr_PlayerMovement : MonoBehaviour
{
    public SO_PlayerMovementSettings movementSettings;
    
    //Saved Controls
    private string horizontalMovementAxisName_;
    private string verticalMovementAxisName_;
    private string horizontalAimAxisName_;
    private string verticalAimAxisName_;

    //Components
    private Scr_PlayerInventory inventory_;
    private Scr_PlayerHealth health_;
    private Scr_PlayerID playerID_;
    private Rigidbody rigidbody_;
    private Animator animator_;
    
    private Vector3 knockbackVelocity_;
    private float stunTimer_;

    void Start()
    {
        playerID_ = GetComponent<Scr_PlayerID>();
        rigidbody_ = GetComponent<Rigidbody>();
        health_ = GetComponent<Scr_PlayerHealth>();
        inventory_ = GetComponent<Scr_PlayerInventory>();
        animator_ = GetComponentInChildren<Animator>();

        SetupControls(playerID_.id);

        rigidbody_.drag = movementSettings.minKnockbackDrag;
    }

    void Update()
    {
        Debug.Log("stun" + stunTimer_);
        Vector3 velocity = new Vector3();

        animator_.SetBool("isWalking", false );
        if (stunTimer_ > 0)
        {
            stunTimer_ -= Time.deltaTime;
        }
        else
        {
            //movement
            Vector2 leftStick = new Vector2();
            leftStick.x = movementSettings.speed * Input.GetAxis(horizontalMovementAxisName_);
            leftStick.y = -movementSettings.speed * Input.GetAxis(verticalMovementAxisName_);
            if (leftStick.magnitude > movementSettings.deadzone)
            {
                animator_.SetBool("isWalking", true);
                velocity.x += leftStick.x;
                velocity.z += leftStick.y;
            }
            //rotation
            Vector2 rightStick = new Vector2(Input.GetAxis(horizontalAimAxisName_), -Input.GetAxis(verticalAimAxisName_));
            if (rightStick.magnitude > movementSettings.deadzone)
            {
                float desiredAngle = Vector2.Angle(Vector2.up, rightStick);
                if (rightStick.x < 0) { desiredAngle = -desiredAngle; }
                float angleChange = desiredAngle - transform.rotation.eulerAngles.y;
                if (angleChange < -180) { angleChange += 360; }
                else if (angleChange > 180) { angleChange -= 360; }
                float angleVelocity = Mathf.Clamp(angleChange, -movementSettings.rotationSpeed, movementSettings.rotationSpeed);
                rigidbody_.angularVelocity = new Vector3(0, angleVelocity, 0);
            }
            else
            {
                rigidbody_.angularVelocity = new Vector3();
            }
        }

        //knockback
        UpdateKnockback();
        velocity += Mathf.Min(knockbackVelocity_.magnitude, movementSettings.maxKnockbackSpeed) * knockbackVelocity_.normalized;

        rigidbody_.velocity = velocity;
    }

    void SetupControls(int playerId)
    {
        horizontalMovementAxisName_ = movementSettings.horizontalMovementAxisName + playerId;
        verticalMovementAxisName_ = movementSettings.verticalMovementAxisName + playerId;

        horizontalAimAxisName_ = movementSettings.horizontalAimAxisName + playerId;
        verticalAimAxisName_ = movementSettings.verticalAimAxisName + playerId;
    }

    void FixedUpdate()
    {
    }

    public void AddKnockback(Vector3 direction, float amount)
    {
        knockbackVelocity_ += direction.normalized * amount * (health_.percentage / 100) * 0.01f;
    }

    public void Stun(float time_)
    {
        stunTimer_ = time_;
    }

    void UpdateKnockback()
    {
        float knockbackDrag = Mathf.Lerp(movementSettings.minKnockbackDrag, movementSettings.maxKnockbackDrag, health_.percentage / 100);
        knockbackVelocity_ *= knockbackDrag;
    }
}
