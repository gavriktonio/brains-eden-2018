﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PlayerUI : MonoBehaviour {

    private GameObject UI_BButtonIcon;
    private GameObject[] UI_DurabilityIcons;

    public Vector3 buttonScale;
    public Vector3 buttonOffset;
    public AnimationCurve buttonAniCurve;
    public float buttonAniSpeed;
    private float buttonAniTimer;

    public Vector3 durabilityScale;
    public Vector3 durabilityOffset;
    public float durabilityJitter;

    [HideInInspector]
    public Scr_PlayerID id;
    public float heldGunDurability;
    public Scr_PlayerInventory invScript;

	// Use this for initialization
	void Start () {
        id = GetComponent<Scr_PlayerID>();
        invScript = GetComponent<Scr_PlayerInventory>();

        //load button UI
        UI_BButtonIcon = Instantiate(Resources.Load("UI_PlaneB") as GameObject, transform.position + buttonOffset, Quaternion.identity, transform);
        ShowMinigameUI(false);

        //load durabilty UI
        UI_DurabilityIcons = new GameObject[5];
        for (int i = 0; i < 5; i++)
        {
            UI_DurabilityIcons[i] = Instantiate(Resources.Load("UI_DurabilityIcons" + i) as GameObject, transform.position + durabilityOffset, Quaternion.identity, transform);
            UI_DurabilityIcons[i].transform.localScale = durabilityScale;
        }

        if (invScript.isHolding)
            ShowDurability(true);
        else
            ShowDurability(false);
    }
	
	// Update is called once per frame
	void Update () {
        //move and scale the button icon
        buttonAniTimer += Time.deltaTime * buttonAniSpeed;
        if(buttonAniTimer >= 2)
            buttonAniTimer = 0;
        UI_BButtonIcon.transform.localScale = buttonScale * buttonAniCurve.Evaluate(buttonAniTimer);
        UI_BButtonIcon.transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);
    }

    public void ShowMinigameUI(bool visible)
    {
        UI_BButtonIcon.SetActive(visible);
    }

    public void ShowDurability(bool visible)
    {
        //hide all
        for (int i = 0; i < 5; i++)
            UI_DurabilityIcons[i].SetActive(false);

        if (visible)
        {
            print(UI_DurabilityIcons[0]);
            //set desired visible icon
            if (heldGunDurability > 80)
                UI_DurabilityIcons[0].SetActive(true);
            else if (heldGunDurability > 60)
                UI_DurabilityIcons[1].SetActive(true);
            else if (heldGunDurability > 40)
                UI_DurabilityIcons[2].SetActive(true);
            else if (heldGunDurability > 20)
                UI_DurabilityIcons[3].SetActive(true);
            else
                UI_DurabilityIcons[4].SetActive(true);
        }

        //move and scale the durability icons
        for (int i = 0; i < 5; i++)
        {
            Vector2 randOffset = Vector2.zero;
            if (heldGunDurability <= 20)
                randOffset = new Vector2(Random.Range(-durabilityJitter, durabilityJitter), Random.Range(-durabilityJitter, durabilityJitter));
            UI_DurabilityIcons[i].transform.position = transform.position + new Vector3(randOffset.x, 0, randOffset.y);
            UI_DurabilityIcons[i].transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);
        }
    }
}
