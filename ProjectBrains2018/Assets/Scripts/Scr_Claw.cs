﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Claw : MonoBehaviour
{
    public GameObject closed;
    public GameObject opened;

    public void Close()
    {
        opened.SetActive(false);
        closed.SetActive(true);
    }

    public void Open()
    { 
        closed.SetActive(false);
        opened.SetActive(true);
    }
}
