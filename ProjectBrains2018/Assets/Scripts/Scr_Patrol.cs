﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Patrol : MonoBehaviour {

    [System.Serializable]
    public struct PatrolPoint
    {
        public float timeToMove;
        public Vector3 pointToMoveTo;
    }

    [SerializeField]
    public PatrolPoint[] patrolPoints;
    public int iterator = 0;

    public Vector3 nextTarget;
    public float rotateBy;
    public float timeToMove;

    public float chaseSpeed;

    private Vector3 initialPos_;
    private Vector3 initialRot_;
    private bool isMovingBack_;
    private float timer_;

    private bool seek_;

    private Scr_GameManager manager;

	// Use this for initialization
	void Start ()
    {
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Scr_GameManager>();
        NextPoint();
        NextPoint();
    }
	
	// Update is called once per frame
	void Update ()
    {
        bool chasing = false;
        Scr_PlayerHealth playerToChase = null;
        foreach (Scr_PlayerID i in manager.players)
        {
            if (i.GetComponent<Scr_PlayerHealth>().percentage > 100)
            {
                chasing = true;
                if (playerToChase == null || playerToChase.percentage < i.GetComponent<Scr_PlayerHealth>().percentage)
                {
                    playerToChase = i.GetComponent<Scr_PlayerHealth>();
                }
            }
        }
        if (!chasing)
        {
            timer_ += Time.deltaTime;
            if (timer_ > timeToMove)
            {
                NextPoint();
            }
            float alpha = timer_ / timeToMove;
            transform.position = Vector3.Lerp(initialPos_, nextTarget, alpha);
            transform.rotation = Quaternion.Euler(initialRot_.x, Mathf.Lerp(initialRot_.y, initialRot_.y + rotateBy, alpha), initialRot_.z);
        }
        else
        {
            Debug.Log("chasing");
            float y = transform.position.y;
            transform.position += (playerToChase.transform.position - transform.position) * chaseSpeed * Time.deltaTime;
            transform.position = new Vector3(transform.position.x, y, transform.position.z);
        }
    }

    void SetNextPoint(PatrolPoint point)
    {
        nextTarget = point.pointToMoveTo;
        timeToMove = point.timeToMove;
        timer_ = 0.0f;

        initialRot_ = transform.rotation.eulerAngles;
        initialPos_ = transform.position;
    }

    void NextPoint()
    {
        SetNextPoint(patrolPoints[iterator]);
        iterator++;
        if (iterator >= patrolPoints.Length)
        {
            iterator = 0;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        foreach (PatrolPoint i in patrolPoints)
        {
            Gizmos.DrawSphere(i.pointToMoveTo, 0.2f);
        }
    }
}
