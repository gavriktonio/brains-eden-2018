﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_EffectManager : MonoBehaviour {

    public AudioClip GameMusic;

    public ParticleSystem psStun;
    public ParticleSystem psForce;
    public ParticleSystem psDamage;
    public ParticleSystem psShoot;
    public ParticleSystem psHit;
    public ParticleSystem psJam;
    public ParticleSystem psBust;

    public AudioClip acPickup;
    public AudioClip[] acShoot;
    public AudioClip acSpot;
    public AudioClip acHit;
    public AudioClip acJam;
    public AudioClip acBust;

    private void Start()
    {
        GetComponent<AudioSource>().clip = GameMusic;
        GetComponent<AudioSource>().Play();
    }
}

