﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_GunSpawner : MonoBehaviour {

    public float spawnJumpForce;
    public float spawnSpinForce;
    public GameObject spawnTriggerBox;
    public GameObject[] weapons;
    public float[] weaponSpawnChance;
    public int weaponsInScene;

    public GameObject confettiBullet;
    public float confettiChance;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Random.Range(0, 100) < weaponSpawnChance[weaponsInScene])
        {
            SpawnWeapon();
            weaponsInScene++;
        }
	}

    void SpawnWeapon()
    {
        Vector3 spawnPos = spawnTriggerBox.transform.position;
        spawnPos += transform.right * Random.Range(-spawnTriggerBox.transform.localScale.x / 2, spawnTriggerBox.transform.localScale.x / 2);
        spawnPos += transform.forward * Random.Range(-spawnTriggerBox.transform.localScale.z / 2, spawnTriggerBox.transform.localScale.z / 2);

        Scr_GunBase newGun = Instantiate(weapons[Random.Range(0, weapons.Length)], spawnPos, Quaternion.identity, null).GetComponent<Scr_GunBase>();

        Rigidbody Gunbody = newGun.GetComponent<Rigidbody>();
        Gunbody.AddTorque(new Vector3(0, Random.Range(-spawnSpinForce, spawnSpinForce), 0), ForceMode.Impulse);
        Gunbody.AddForce(new Vector3(0, spawnJumpForce, 0), ForceMode.Impulse);

        if (Random.Range(0, 100) < confettiChance)
        {
            newGun.bulletKind = confettiBullet;
            newGun.bulletLifetime = new Vector2(0, 0);
        }
    }
}
