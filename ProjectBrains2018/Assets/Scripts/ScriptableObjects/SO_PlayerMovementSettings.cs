﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerSettings", menuName = "Player/Settings", order = 1)]
public class SO_PlayerMovementSettings : ScriptableObject
{
    public float speed;
    public float maxKnockbackSpeed;
    public float minKnockbackDrag;
    public float maxKnockbackDrag;

    public float rotationSpeed;
    public float deadzone;

    public string horizontalMovementAxisName;
    public string verticalMovementAxisName;
    public string horizontalAimAxisName;
    public string verticalAimAxisName;
}
