﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_CctvDetection : MonoBehaviour
{
    public float timeToDetect;
    public float multiplyTimeToDrag;
    public float floorHeight;
    public float outOfScreenHeight;


    public List<Scr_PlayerID> detectedPlayers_ = new List<Scr_PlayerID>();
    public float timer_ = 0.0f;
    private GameObject holdingPlayer_;

    private Scr_GameManager gameManager_;
    private Light mainLight_;
    private Light secondLight_;
    private Scr_Claw claw_;

    private void Start()
    {
        gameManager_ = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Scr_GameManager>();
        mainLight_ = transform.parent.GetComponent<Light>();
        secondLight_ = GetComponent<Light>();
        claw_ = GetComponentInChildren<Scr_Claw>();
    }

    void Update()
    {
        if (holdingPlayer_ == null)
        {
            claw_.Open();
            if (detectedPlayers_.Count > 0)
            {
                timer_ += Time.deltaTime;
                timer_ = Mathf.Min(timer_, timeToDetect);
                mainLight_.color = Color.red;
            }
            else
            {
                timer_ -= Time.deltaTime * multiplyTimeToDrag;
                timer_ = Mathf.Max(timer_, 0);
                mainLight_.color = Color.white;
            }
            SetClawPos(timer_ / timeToDetect);
            if (timer_ >= timeToDetect)
            {
                timer_ = timeToDetect;
                holdingPlayer_ = detectedPlayers_[0].gameObject;
                detectedPlayers_.Remove(detectedPlayers_[0]);
            }
        }
        else
        {
            claw_.Close();
            timer_ -= Time.deltaTime * multiplyTimeToDrag;
            SetClawPos(timer_ / multiplyTimeToDrag);
            holdingPlayer_.transform.position = claw_.transform.position + Vector3.up * 0.4f;
            if (timer_ < 0)
            {
                gameManager_.PlayerSpotted(holdingPlayer_.GetComponent<Scr_PlayerID>());
                holdingPlayer_ = null;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Scr_PlayerID freshlyDetectedPlayer = other.GetComponent<Scr_PlayerID>();
        if (freshlyDetectedPlayer != null)
        {
            if (detectedPlayers_.Contains(freshlyDetectedPlayer))
            {
                Debug.LogError("Player has multiple colliders!");
            }
            detectedPlayers_.Add(other.GetComponent<Scr_PlayerID>());
        }

    }

    void OnTriggerExit(Collider other)
    {
        Scr_PlayerID exitedPlayer = other.GetComponent<Scr_PlayerID>();
        detectedPlayers_.Remove(other.GetComponent<Scr_PlayerID>());
    }

    void SetClawPos (float alpha)
    {
        Vector2 clawPos;
        if (detectedPlayers_.Count > 0)
        {
            clawPos = Vector2.Lerp(new Vector2(claw_.transform.position.x, claw_.transform.position.z), new Vector2(detectedPlayers_[0].transform.position.x, detectedPlayers_[0].transform.position.z), 0.13f);
        }
        else
        {
            clawPos = new Vector2(claw_.transform.position.x, claw_.transform.position.z);
        }
        float clawHeight;
        if (holdingPlayer_ == null)
        {
            clawHeight = Mathf.Lerp(outOfScreenHeight, floorHeight, alpha);
        }
        else
        {
            clawHeight = Mathf.SmoothStep(outOfScreenHeight, floorHeight, alpha);
        }
        claw_.transform.position = new Vector3(clawPos.x, clawHeight, clawPos.y);
    }

}
