﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Scr_GameManager : MonoBehaviour {

    public List<Scr_PlayerID> players = new List<Scr_PlayerID>();
    private bool gameEnd;

	// Use this for initialization
	void Start ()
    {
        GameObject[] playerObjects = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject i in playerObjects)
        {
            players.Add(i.GetComponent<Scr_PlayerID>());
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = players.Count - 1; i >= 0; i--)
        {
            if (players[i] == null)
            {
                players.RemoveAt(i);
            }
        }
        if (gameEnd)
        {
            if (Input.GetButtonDown("Start"))
            {
                SceneManager.LoadScene("MainMenu");
            }
        }
	}

    public void PlayerSpotted(Scr_PlayerID player)
    {
        Debug.Log(player.id);
        players.Remove(player);
        Destroy(player.gameObject);
        if (players.Count < 2)
        {
            Debug.Log("Win");
            Text HealthUI = GameObject.FindGameObjectWithTag("Canvas").transform.GetChild(0).GetChild(players[0].id).GetComponent<Text>();
            GameObject winscreen = GameObject.FindGameObjectWithTag("Canvas").transform.GetChild(1).gameObject;
            winscreen.SetActive(true);
            Text playerwon = winscreen.transform.GetChild(0).GetComponent<Text>();
            playerwon.text = "PLAYER " + players[0].id + " WON!";
            playerwon.color = HealthUI.color;
            gameEnd = true;
        }
    }
}
