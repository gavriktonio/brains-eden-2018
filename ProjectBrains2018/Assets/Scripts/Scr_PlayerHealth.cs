﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scr_PlayerHealth : MonoBehaviour
{
    private Scr_PlayerID PlayerID;
    private Text HealthUI;

    public float percentage;

    void Start()
    {
        PlayerID = GetComponent<Scr_PlayerID>();
        HealthUI = GameObject.FindGameObjectWithTag("Canvas").transform.GetChild(0).GetChild(PlayerID.id).GetComponent<Text>();
    }

    public void Damage(float damage)
    {
        percentage += damage;
        HealthUI.text = percentage.ToString();
    }
}
