﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_Minigames : MonoBehaviour {
    
    private int progressPercentage;

    private string input;

    private Scr_GunBase gunScript;
    private Scr_PlayerUI[] playerUIScripts;

    // Use this for initialization
    private void Start()
    {
        gunScript = GetComponent<Scr_GunBase>();
        playerUIScripts = FindObjectsOfType<Scr_PlayerUI>();
    }

    public void StartMinigame()
    { 
        progressPercentage = 0;
        ShowUI(true);
    }
	
    public void PlayMinigame()
    {
        Debug.Log("Minigame");
        if (gunScript.playerId != null)
        {
            if (Input.GetButtonDown(gunScript.playerId.GetComponent<Scr_PlayerInventory>().bButton_))
                progressPercentage += 10;
            if (progressPercentage >= 100)
                Completed();
        }
    }

    void Completed()
    {
        gunScript.gunState = GunStates.working;
        ShowUI(false);
    }

    void ShowUI(bool visible)
    {
        for(int i = 0; i < playerUIScripts.Length; i++)
        {
            if (playerUIScripts[i].id == gunScript.playerId)
                playerUIScripts[i].ShowMinigameUI(visible);
        }
    }
}
