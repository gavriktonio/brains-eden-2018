﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_ItemBase : MonoBehaviour {

    public bool isPickedUp = false;
    [HideInInspector]
    public int userID;
    public Scr_PlayerID playerId;
    protected Rigidbody itemBody;

    public void HeldBehaviour()
    {
        Vector3 playerForward = playerId.transform.right;
        playerForward.y = 0;
        transform.GetChild(0).rotation = Quaternion.Euler(0, playerId.transform.rotation.eulerAngles.y - 90, 0);
    }

    public void DroppedBehaviour()
    {
    }

    public virtual void UseItem(int playerID, Quaternion rotation)
    {
        print("Don't assign me as a component, use one of my child classes.");
    }

    public void GetTossed(Vector3 tossDir, float tossForce)
    {
        itemBody.AddForce(tossDir * tossForce, ForceMode.Impulse);
    }

    public void GetPickedUp(Scr_PlayerID id)
    {
        transform.parent = id.transform.GetComponentInChildren<Scr_GunSocket>().transform;
        transform.localPosition = new Vector3();
        transform.localRotation = Quaternion.identity;
        itemBody.isKinematic = true;
        isPickedUp = true;
        playerId = id;
    }

    public void GetDropped()
    {
        itemBody.isKinematic = false;
        transform.parent = null;
        isPickedUp = false;
        itemBody.velocity = new Vector3(0, Random.Range(1.0f, 2.0f), 0);
        itemBody.angularVelocity = new Vector3(Random.Range(-4.0f, 4.0f), Random.Range(-4.0f, 4.0f), Random.Range(-4.0f, 4.0f));
    }
}
