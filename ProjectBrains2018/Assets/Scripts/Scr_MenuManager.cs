﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scr_MenuManager : MonoBehaviour
{
    public string sceneName;
    bool[] players = {false, false, false, false};
    private List<GameObject> playerJoinIndications_ = new List<GameObject>();
    private List<GameObject> playerBoxes = new List<GameObject>();
    private GameObject StartGameText_;
    public GameObject spawnParticles;
    public Vector3 spawnParticesOffset;

    bool GameStarted_ = false;


    private void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        DontDestroyOnLoad(gameObject);
        for (int i = 0; i < 4; i++)
        {
            playerJoinIndications_.Add(GameObject.Find("PlayerIndications").transform.GetChild(i).gameObject);
        }
        for (int i = 0; i < 4; i++)
        {
            playerBoxes.Add(GameObject.Find("PlayerBoxes").transform.GetChild(i).gameObject);
        }
        StartGameText_ = GameObject.Find("Canvas").transform.GetChild(1).gameObject;
    }

    private void Update()
    {
        if (!GameStarted_)
        {
            int playersInGame = 0;
            for (int i = 0; i < 4; i++)
            {
                if (Input.GetButtonDown("FaceButtonA" + i))
                {
                    if (spawnParticles != null)
                    {
                        Instantiate(spawnParticles, playerJoinIndications_[i].transform.GetChild(0).position + spawnParticesOffset, Quaternion.identity);
                    }
                    players[i] = !players[i];
                    playerJoinIndications_[i].SetActive(players[i]);
                    playerBoxes[i].SetActive(!players[i]);
                }
                if (players[i])
                {
                    playersInGame++;
                }
            }
            if (playersInGame > 1)
            {
                StartGameText_.SetActive(true);
                if (Input.GetButtonDown("Start"))
                {
                    LaunchGame();
                }
            }
            else
            {
                StartGameText_.SetActive(false);
            }
        }
    }

    void LaunchGame()
    {
        GameStarted_ = true;
        SceneManager.LoadScene(sceneName);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Scene loaded");
        GameObject[] playerCharacters = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 3; i >= 0; i--)
        {
            Scr_PlayerID id = playerCharacters[i].GetComponent<Scr_PlayerID>();
            if (players[id.id] == false)
            {
                Debug.Log(id.id);
                Destroy(playerCharacters[i]);
            }
        }
        Destroy(gameObject);
    }
}
