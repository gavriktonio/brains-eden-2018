﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GunStates { working = 0, jammed = 1, busted = 2}

public class Scr_GunBase : Scr_ItemBase
{
    public float spawnSize;
    public float durability;
    public float durabilityDecrease;
    public float reliability;
    public GunStates gunState;

    //gun variables
    public float fireRate;
    private float fireDelayTimer;
    public Vector2 bulletLifetime;
    public Vector2 bulletSpeed;
    public int bulletAmount;
    public float bulletSpread;
    public float bulletAccuracy;
    public GameObject bulletKind;
    public Vector3 bulletSpawnOffset;

    private Scr_Minigames minigameScript;
    private Scr_PlayerUI[] playerUIScripts;
    private Scr_EffectManager particleManager;
    private ParticleSystem psBust;
    private AudioSource gunAudio;
    private float bustDestroyDelay = 3;

    void Start()
    {
        transform.localScale = Vector3.one * 0.001f;
        itemBody = GetComponent<Rigidbody>();
        minigameScript = GetComponent<Scr_Minigames>();
        particleManager = FindObjectOfType<Scr_EffectManager>();
        gunAudio = GetComponent<AudioSource>();

        //create smoke particle
        psBust = Instantiate(particleManager.psBust, transform.position, transform.rotation, transform);
        psBust.Stop();
        
        //find player scripts
        Scr_PlayerUI[] foundScripts = FindObjectsOfType<Scr_PlayerUI>();
        print(foundScripts[0]);
        playerUIScripts = new Scr_PlayerUI[foundScripts.Length];
        for(int pS = 0; pS < playerUIScripts.Length; pS++)
        {
            for(int fS = 0; fS < foundScripts.Length; fS++)
            {
                if (foundScripts[fS].id.id == pS)
                    playerUIScripts[pS] = foundScripts[fS];
            }
        }
    }

    void Update()
    {
        //spawn size increase
        if (transform.localScale.x < 1)
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one * spawnSize, 0.1f);

        //manage reliability
        reliability = durability;

        //call item behaviours
        if (isPickedUp)
        {
            HeldBehaviour();
            //call minigame
            if (gunState == GunStates.jammed)
                minigameScript.PlayMinigame();

            playerUIScripts[userID].heldGunDurability = durability;
        }

        //destroy when below level
        if (transform.position.y < -10)
            RemoveSelf();

        //destroy when busted
        if (gunState == GunStates.busted)
        {
            bustDestroyDelay -= Time.deltaTime;
            if (bustDestroyDelay < 0)
                RemoveSelf();
        }

    }

    public override void UseItem(int playerID, Quaternion rotation)
    {
        fireDelayTimer -= Time.deltaTime;
        if (fireDelayTimer <= 0 && gunState == GunStates.working)
        {
            //get jammed
            if(Random.Range(0, 100) > reliability)
            {
                gunState = GunStates.jammed;
                minigameScript.StartMinigame();
            }
            //get busted
            else if(Random.Range(0, 100) > reliability)
            {
                gunState = GunStates.busted;
                //particle and audio
                psBust.Play();
                gunAudio.clip = particleManager.acBust;
                gunAudio.Play();
            }
            else
            {
                fireDelayTimer = fireRate;
                for (int i = 0; i < bulletAmount; i++)
                {
                    Scr_BulletBase newBullet = Instantiate(bulletKind, transform.position, rotation, null).GetComponent<Scr_BulletBase>();
                    float bulletAngleOffset = (bulletSpread * (bulletAmount - 1)) / 2 - (bulletSpread * i);
                    bulletAngleOffset += Random.Range(-bulletAccuracy, bulletAccuracy);
                    Quaternion newRot = Quaternion.Euler(0, rotation.eulerAngles.y + bulletAngleOffset, 0);
                    newBullet.Fire(playerId, newRot, Random.Range(bulletLifetime.x, bulletLifetime.y), Random.Range(bulletSpeed.x, bulletSpeed.y));
                    //particle and audio
                    Instantiate(particleManager.psShoot, transform.position, transform.rotation, null);
                    gunAudio.clip = particleManager.acShoot[Random.Range(0, particleManager.acShoot.Length)];
                    gunAudio.pitch = Random.Range(0.8f, 1.2f);
                    gunAudio.Play();
                }
                durability -= durabilityDecrease;
            }
        }
    }

    public void RemoveSelf()
    {
        FindObjectOfType<Scr_GunSpawner>().weaponsInScene--;
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GunKillBox")
            RemoveSelf();
    }
}
