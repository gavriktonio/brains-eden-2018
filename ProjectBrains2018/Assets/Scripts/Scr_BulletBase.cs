﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_BulletBase : MonoBehaviour {

    public Scr_PlayerID userID;
    public float bulletSize;
    public float damage;
    public float stunStrenght;
    public float stunTime;
    public float force;
    public float forceRadius;
    public ParticleSystem[] destroyParticles;

    private float lifetime;
    private float speed;
    private bool fired;

    public void Start()
    {
        transform.localScale = Vector3.one * 0.001f;
    }

    public void Update()
    {
        //spawn size increase
        transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one * bulletSize, 0.3f);

        if (fired)
        {
            transform.position += (transform.forward * speed);
            lifetime -= Time.deltaTime;
            if (lifetime < 0)
            {
                Explode();
                Destroy(gameObject);
            }
        }
    }

    public void Fire(Scr_PlayerID id, Quaternion bulletRot, float bulletLifetime, float bulletSpeed)
    {
        userID = id;
        transform.rotation = bulletRot;
        lifetime = bulletLifetime;
        speed = bulletSpeed;
        fired = true;
    }

    public void Impact(Scr_PlayerID hitPlayerID)
    {
        if (hitPlayerID != null)
        {
            if (hitPlayerID != userID)
            {
                Explode(); //Adds knockback
                hitPlayerID.GetComponent<Scr_PlayerMovement>().Stun(stunTime);
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);
        if (other.gameObject.layer == 10)
            Impact(other.gameObject.GetComponent<Scr_PlayerID>());
        else
            Impact(null);
    }

    private void Explode()
    {
        Scr_GameManager gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Scr_GameManager>();
        foreach (Scr_PlayerID i in gameManager.players)
        {
            if (i.GetComponent<Scr_PlayerID>() != userID)
            {
                Vector3 toPlayer = i.transform.position - transform.position;
                if (toPlayer.magnitude < forceRadius)
                {
                    if (toPlayer.magnitude < forceRadius / 2)
                    {
                        i.GetComponent<Scr_PlayerHealth>().Damage(damage);
                    }
                    i.GetComponent<Scr_PlayerMovement>().AddKnockback(i.transform.position - transform.position, force * (forceRadius - toPlayer.magnitude));
                }
            }
        }
        for (int i = 0; i < destroyParticles.Length; i++)
        {
            Instantiate(destroyParticles[i], transform.position, transform.rotation, null);
        }
    }
}
